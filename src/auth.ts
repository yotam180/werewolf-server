import * as admin from "firebase-admin"

admin.initializeApp({
    credential: admin.credential.applicationDefault(),
    databaseURL: null // TODO: Might need this at a later point
})

export async function verifyToken(token: string): Promise<Partial<admin.auth.DecodedIdToken>> {
    try {
        return await admin.auth().verifyIdToken(token)
    }
    catch (_) {
        console.error("Error: ", _)
        return null
    }
}
