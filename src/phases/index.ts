// import { ArraySchema, Schema, type } from "@colyseus/schema"
// import { RoomState } from "../roomState"

// export enum GamePhase {
//     WAITING = "WAITING",
//     STARTING_GAME = "STARTING_GAME",
// }

// export class GameState extends Schema {
//     @type("string")
//     phase: GamePhase

//     state: RoomState

//     constructor(phase: GamePhase, state: RoomState) {
//         super()
//         this.phase = phase
//         this.state = state
//         console.log("constructed")
//     }

//     onLoop(): GameState | null { return null }
//     onStart() {}
//     onEnd() {}
// }

// export class WaitingState extends GameState {
//     @type(["string"])
//     readyPlayers = new ArraySchema<string>()

//     constructor(state: RoomState) {
//         super(GamePhase.WAITING, state)
//     }

//     onStart() {
//         this.readyPlayers.splice(0, this.readyPlayers.length) // Clearing the list
//     }

//     onLoop(): GameState | null {
//         // if (this.readyPlayers.length === this.state.players.length) {
//         //     // TODO: Return next state
//         // }

//         return null
//     }
// }

