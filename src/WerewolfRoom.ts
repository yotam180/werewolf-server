import { Room, ServerError, ErrorCode } from "colyseus"
import { verifyToken } from "./auth"

import { WerewolfClient } from './interfaceManager';
import { Player } from './Player';
import { RoomState } from './RoomState';
import { optionsValidation } from './options';
import { generateID } from './utils';

export class WerewolfRoom extends Room<RoomState> {
  maxClients = 16;

  async generateUniqueID(length: number = 4): Promise<string> {
    let tries = 20;
    let id = generateID(length);

    while (await this.presence.exists(id)) {
      id = generateID(length);
      tries--;

      if (tries == 0 && length < 9) {
        tries = 20;
        length++;
      }
    }
    return id;
  }

  async onCreate(_options: any) {
    this.roomId = await this.generateUniqueID()
    this.setState(new RoomState())
    this.setSimulationInterval(this.onLoop.bind(this), 500)

    this.onMessage("ready", (client) => this.state.playerReady(client.sessionId));
    this.onMessage("not_ready", (client) => this.state.playerNotReady(client.sessionId));
    this.onMessage("set_roles", (_, message) => this.updateRolesBank(null, message));
  }

  async onAuth(_client: WerewolfClient, options: any) {
    if (!optionsValidation(options)) {
      throw new ServerError(ErrorCode.AUTH_FAILED, "Invalid options field")
    }

    const auth = await verifyToken(options.token)

    if (!auth) {
      throw new ServerError(ErrorCode.AUTH_FAILED, "Invalid authentication token")
    }

    return {
      token: auth,
      info: options.info
    }
  }

  onJoin(client: WerewolfClient, _options: any) {
    this.state.players[client.sessionId] = new Player(client.auth.info.nickname, client.sessionId)
  }

  onLeave(client: WerewolfClient, _consented: boolean) {
    delete this.state.players[client.sessionId]
  }

  onDispose() {
  }

  onLoop(deltaTime: number) {
    this.state.onLoop(deltaTime / 1000.0)
  }

  updateRolesBank(_: WerewolfClient, message: any) {
    // TODO: Validate that a client is the master client
  
    if (message["roles"]) {
      let roles: string[] = message["roles"]
      this.state.setRolesBank(roles)
    }
  }
}
