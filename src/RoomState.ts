import { ArraySchema, MapSchema, Schema, type } from "@colyseus/schema"
import { Player, SignalState } from "./Player";
import { Town } from "./logic/town";
import { GeneratorType } from "./logic/generators";

enum GamePhase {
    WAITING = "WAITING",
    STARTING = "STARTING",
    ROLE_SHUFFLE = "ROLE_SHUFFLE",
    NIGHT_TRANSITION = "NIGHT_TRANSITION"
}

export class RoomState extends Schema {
    @type({map: Player})
    players = new MapSchema<Player>();

    @type("string")
    phase: GamePhase = GamePhase.WAITING

    @type(["string"])
    rolesBank = new ArraySchema<string>()

    @type("number")
    timer: number = 0

    town: Town | null = null

    MINIMUM_PLAYERS = 1

    clearSignals() {
        for (var p in this.players) {
            this.players[p].signalState = SignalState.NONE
        }
    }

    playersArr(): Player[] {
        let arr: Player[] = []
        for (var p in this.players) {
            arr.push(this.players[p])
        }
        return arr
    }

    getPlayer(playerId: string): Player {
        return this.players[playerId]
    }

    setRolesBank(roles: string[]) {
        if (this.phase !== GamePhase.WAITING) return;
        this.rolesBank = new ArraySchema<string>(...roles)
    }

    startTimer(time: number) {
        this.timer = time
    }

    resetTimer() {
        this.timer = 0
    }

    advanceTimer(deltaTime: number) {
        this.timer -= deltaTime
    }

    timerDue(): boolean {
        if (this.timer < 0) {
            this.resetTimer()
            return true
        }

        return false
    }

    syncToState() {
        if (!this.town) return

        for (const c of this.town.charactersArr()) {
            if (!this.getPlayer(c.id)) continue

            this.getPlayer(c.id).nickname = c.name
            this.getPlayer(c.id).alive = c.isAlive()
            this.getPlayer(c.id).role = c.role.type
        }
    }

    onLoop(deltaTime: number) {
        if (this.timer > 0) {
            this.timer -= deltaTime
        }
        if ((this as any)["onLoop_" + this.phase]) {
            (this as any)["onLoop_" + this.phase]()
        }
    }

    setState_WAITING() {
        this.phase = GamePhase.WAITING
        this.town = null
        this.resetTimer()
        this.clearSignals()
    }

    onLoop_WAITING() {
        // In the waiting state, if a player is signaled, it means it is ready to start the game.
        if (this.playersArr().filter(p => p.signalState !== SignalState.TRUE).length === 0 && this.playersArr().length >= this.MINIMUM_PLAYERS) {
            this.setState_STARTING();
        }
    }

    setState_STARTING() {
        this.phase = GamePhase.STARTING
        this.startTimer(7)
        this.clearSignals()

        const players = this.playersArr().map(({ id, nickname }) => ({ name: nickname, id }))
        this.town = new Town(players, [GeneratorType.VILLAGER, GeneratorType.VILLAGER, GeneratorType.VILLAGER]) // TODO: Real generators
        this.syncToState()
    }

    onLoop_STARTING() {
        if (this.timerDue()) {
            this.setState_ROLE_SHUFFLE();
        }
    }
    
    setState_ROLE_SHUFFLE() {
        this.phase = GamePhase.ROLE_SHUFFLE
        this.startTimer(7)
    }

    onLoop_ROLE_SHUFFLE() {
        if (this.timerDue()) {
            this.setState_NIGHT_TRANSITION()
        }
    }

    setState_NIGHT_TRANSITION() {
        this.phase = GamePhase.NIGHT_TRANSITION
    }

    playerReady(player_id: string) {
        if (this.phase !== GamePhase.WAITING) return;
        if (!this.players[player_id]) return;
        this.players[player_id].signalState = SignalState.TRUE;
    }

    playerNotReady(player_id: string) {
        if (this.phase !== GamePhase.WAITING) {
            return;
        }

        if (!this.players[player_id]) {
            return;
        }

        this.players[player_id].signalState = SignalState.FALSE
    }
}
