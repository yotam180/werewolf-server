import { Client } from "colyseus";
import * as admin from "firebase-admin"

interface UserInfo {
    nickname: string
}

interface WerewolfClient extends Client {
    auth: {
        token: admin.auth.DecodedIdToken,
        info: UserInfo
    }
}

export {
    UserInfo,
    WerewolfClient
}