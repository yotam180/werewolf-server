import { Role, RoleType } from "./roles/role"
import { Town } from "./town"
import { Attack, Defense } from "./attack"
import { Action } from "./action"
import { Power } from "./power"
import { RoleCreators } from "./roles/creators"

export interface CharacterInfo {
    name: string,
    id: string
}

export class Character {
    id: string
    name: string
    role: Role
    town: Town

    attacks: Attack[] = []
    defenses: Defense[] = []
    action: Action = Action.Empty()

    deathNight: number | null = null

    constructor(info: CharacterInfo, role: RoleType, town: Town) {
        this.id = info.id
        this.name = info.name
        this.role = new (RoleCreators[role])(this)
        this.town = town
    }

    isAlive(): boolean {
        return this.deathNight === null
    }

    resetNight() {
        this.attacks = []
        this.defenses = []
        this.action = Action.Empty()
    }

    addAttack(attack: Attack) {
        this.attacks.push(attack)
    }

    addDefense(defense: Defense) {
        this.defenses.push(defense)
    }

    setAction(action: Action) {
        this.action = action
    }

    maxAttack(): Attack {
        if (this.attacks.length === 0) {
            return new Attack(this, Power.NONE, " if you see this it's a bug")
        }

        this.attacks.sort((a, b) => b.power - a.power)
        return this.attacks[0]
    }

    maxDefense(): Defense {
        this.defenses.sort((a, b) => b.power - a.power)
        if (this.defenses.length === 0 || this.defenses[0].power <= this.role.basicDefense) {
            return new Defense(this, this.role.basicDefense, " were too strong")
        }

        return this.defenses[0]
    }

    die() {
        this.deathNight = this.town.currentNight
    }
}
