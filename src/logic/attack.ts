import { Character } from "./character"
import { Power } from "./power"

class Interaction {
    from: Character
    power: Power
    description: string

    constructor(from: Character, power: Power, description: string) {
        this.from = from
        this.power = power
        this.description = description
    }
}

/* tslint:disable: max-classes-per-file */
export class Attack extends Interaction {; }
export class Defense extends Interaction {; }
