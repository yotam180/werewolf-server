import { Character, CharacterInfo } from "./character";
import { GeneratorType, createTown } from "./generators";
import { Power } from "./power";
import { Action } from "./action";
import { AlphaWerewolf } from "./roles/alpha_werewolf";
import { RoleType } from "./roles/role";

import {} from "./roles/villager"
import {} from "./roles/alpha_werewolf"
import {} from "./roles/werewolf"

/*
Things to finish here:
1. Alignment and factions + victory calculation
2. Limits on roles (e.g. max 1 Alpha Werewolf)
3. Night stages?
4. Idle players (that should be killed next night)
*/

export class Town {
    characters: Character[] = []
    currentNight: number = 0

    LIMITS = {
        [RoleType.ALPHA_WEREWOLF]: 1
    }

    constructor(characters: CharacterInfo[], roles: GeneratorType[]) {
        if (roles.length < characters.length) {
            throw new Error(`Cannot create town with ${characters.length} characters and only ${roles.length} roles`)
        }

        roles.splice(characters.length, roles.length - characters.length)

        const townRoles = createTown(roles, this.LIMITS)
        // shuffleArray(townRoles) // TODO: RETURN THIS (FUCKING IMPORTANT)
        for (let i = 0; i < characters.length; i++) {
            this.addCharacter(new Character(characters[i], townRoles[i], this))
        }
    }

    public getCharacter(id: string): Character {
        for (const c of this.characters) {
            if (c.id === id) {
                return c
            }
        }
        throw new Error(`Character ${id} not found`)
    }

    public* charactersArr(): Generator<Character> {
        for (const c of this.characters) {
            yield c
        }
    }

    private addCharacter(c: Character) {
        this.characters.push(c)
    }

    private doActions() {
        for (const character of this.characters) {
            if (character.role.shouldAct()) {
                character.role.preAction()
            }
        }

        for (const character of this.characters) {
            if (character.role.shouldAct()) {
                character.role.doAction()
            }
        }

        for (const character of this.characters) {
            if (character.role.shouldAct()) {
                character.role.postAction()
            }
        }
    }

    private calculateDeaths(): string[] {
        const deathLog: string[] = []

        for (const c of this.characters) {
            console.log(c)
            const attack = c.maxAttack()
            const defense = c.maxDefense()
            if (attack.power > defense.power) {
                c.die()
                deathLog.push(
                    `We found ${c.name} dead in their house tonight`,
                    `Apparently, they ${attack.description}`, // TODO: Format this better (include all attacks)
                    `Rest in peace, ${c.name}`
                )
                continue
            }

            if (attack.power > Power.NONE && defense.from !== c) {
                deathLog.push(`${c.name} was apparently attacked tonight but they survived`)
            }
        }
        return deathLog
    }

    calculateNightResults() { // TODO: Finish?
        this.doActions()
        const deathLog = this.calculateDeaths()
        console.log(deathLog)
    }

    setPlayerAction(playerId: string, ...targets: (boolean | string)[]) {
        const realTargets = targets.map(t => typeof t === "boolean" ? t : this.getCharacter(t))
        this.getCharacter(playerId).setAction(new Action(...realTargets))
    }

    startNight() {
        // TODO: Check if there's something missing
        for (const c of this.characters) {
            c.resetNight()
        }
    }

    nextNightStage() {
        // TODO: Maybe this should return false if the night is over?    
    }

    getAtiveCharacters(): Character[] {
        return [] // TODO
    }
}
