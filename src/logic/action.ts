import { Character } from "./character";

export type Target = Character | boolean;

export class Action {
    targets: Target[]

    // Example targets:
    // For werewolf: [Character]
    // For witch: [Character, Character]
    // For veteran: [boolean]

    constructor(...targets: Target[]) {
        this.targets = targets
    }

    static Empty(): Action {
        return new Action()
    }

    shouldAct(): boolean {
        return this.targets.length > 0 && this.targets[0] !== false
    }

    private targetAt(index: number): Character {
        if (this.targets.length < index || !(this.targets[index] instanceof Character)) {
            throw Error(`No target at ${index}`)
        }
        return this.targets[index] as Character // TODO: Shit
    }

    firstTarget() {
        return this.targetAt(0)
    }

    secondTarget() {
        return this.targetAt(1)
    }
}
