import { Town } from "./town"
import { createTown, GeneratorType } from "./generators"
import { Villager } from "./roles/villager"
import { Werewolf } from "./roles/werewolf"
import { RoleType } from "./roles/role"

/// ---  ///;

const t = createTown([GeneratorType.VILLAGER], {
  [RoleType.ALPHA_WEREWOLF]: 1
})
console.log(t)
