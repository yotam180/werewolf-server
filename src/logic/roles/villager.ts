import { Role, RoleType } from "./role";
import { Character } from "../character";

export class Villager extends Role {
    type = RoleType.VILLAGER
    constructor(character: Character) {
        super(character)
    }
}
