import { Power } from "../power"
import { Character } from "../character"

export enum RoleType {
    VILLAGER = "VILLAGER",
    HEALER = "HEALER",
    FORTUNE_TELLER = "FORTUNE_TELLER",
    WITCH = "WITCH",
    JESTER = "JESTER",
    WEREWOLF = "WEREWOLF",
    ALPHA_WEREWOLF = "ALPHA_WEREWOLF"
}

export abstract class Role {
    character: Character
    type: RoleType = RoleType.VILLAGER

    basicDefense: Power = Power.NONE

    constructor(character: Character) {
        this.character = character
    }

    getTown() {
        return this.character.town
    }

    shouldAct(): boolean  {
        return this.character.action.shouldAct()
    }

    // Prepare stuff here (role blockings, redirects, etc...)
    preAction() {; }

    // Perform the main action of the character.
    // Visit other players here
    doAction() {; }

    // Respond to visits here (e.g. veteran)
    // Refrain from making visits here
    postAction() {; }

    // onDeath() {; }
    // onNightEnd() {; }
}
