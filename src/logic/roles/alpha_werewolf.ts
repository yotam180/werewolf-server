import { Role, RoleType } from "./role"
import { Character } from "../character"
import { Attack } from "../attack"
import { Power } from "../power"

export class AlphaWerewolf extends Role {
    type = RoleType.ALPHA_WEREWOLF
    constructor(character: Character) {
        super(character)
    }

    doAction() {
        this.character.action.firstTarget()
            .addAttack(new Attack(this.character, Power.BASIC, "were attacked by a werewolf"))
    }
}
