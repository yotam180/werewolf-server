import { Character } from "../character";
import { Role, RoleType } from "./role";
import { Villager } from "./villager"
import { Werewolf } from "./werewolf"
import { AlphaWerewolf } from "./alpha_werewolf"

export const RoleCreators: { [key: string]: new(c: Character) => Role } = {
    [RoleType.VILLAGER]: Villager,
    [RoleType.WEREWOLF]: Werewolf,
    [RoleType.ALPHA_WEREWOLF]: AlphaWerewolf
}
