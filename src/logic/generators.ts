import { RoleType } from "./roles/role"

function choice<T>(arr: T[]) {
    const rand = Math.floor(Math.random() * arr.length)
    return arr[rand]
}

type Limits = { [key: string]: number }

export function createTown(generators: GeneratorType[], limits: Limits): RoleType[] {
    const currentLimits: Limits = {}

    // We first have to handle the generators with the least amount of options (those are the most problematic ones)
    generators = generators.concat()
    generators.sort((a, b) => a.length - b.length)

    function get(dict: {[key: string]: number}, value: string) {
        return dict[value] === undefined ? 0 : dict[value]
    }

    return generators.map(g => {

        const generator = Generators[g]
        if (!generator) {
            throw new Error("Could not find generator for " + g);
        }
        
        const validConstructors = generator.filter(con => !get(limits, con) || get(currentLimits, con) < get(limits, con))
        if (validConstructors.length === 0) {
            throw new Error("Could not generate a town") // TODO: Add detailed reason
        }

        const constructor = choice(validConstructors)
        currentLimits[constructor] = get(currentLimits, constructor) + 1
        return constructor
    })
}

export enum GeneratorType {
    VILLAGER = "VILLAGER",
    WEREWOLF = "WEREWOLF",
    RANDOM = "RANDOM"
}

const Generators: { [name: string]: RoleType[] } = {
    [GeneratorType.VILLAGER]: [RoleType.VILLAGER],
    [GeneratorType.WEREWOLF]: [RoleType.WEREWOLF],
    [GeneratorType.RANDOM]: [RoleType.VILLAGER, RoleType.WEREWOLF],
}
