export enum Power {
    NONE,
    BASIC,
    POWERFUL,
    UNSTOPPABLE,
    INVINCIBLE,
}
