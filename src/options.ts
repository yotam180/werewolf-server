import { UserInfo } from './interfaceManager';

interface Options {
    info: UserInfo,
    token: string
}

function optionsValidation(obj: any): obj is Options {
    return obj && typeof obj.token === 'string' && obj.info && typeof obj.info.nickname === 'string';
}

export { 
    Options, 
    optionsValidation
}