function random(min: number, max: number) {
    return Math.round(Math.random() * (max - min) + min)
}

function generateID(length: number = 4): string {
    return random(Math.pow(10, length - 1), Math.pow(10, length)).toString().padStart(9, "0");
}

export {
    random, 
    generateID
}