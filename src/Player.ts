import { Schema, type } from "@colyseus/schema"

export enum SignalState {
  FALSE = 0,
  TRUE = 1,
  NONE = 2,
}

export class Player extends Schema {
    @type("string")
    nickname: string;
  
    @type("string")
    id: string;

    @type("number")
    signalState: SignalState = SignalState.NONE

    @type("boolean")
    alive: boolean = true

    @type("string")
    role: string = null // TODO: We might not want to share this
  
    constructor(nickname: string, id: string) {
      super()
      this.nickname = nickname;
      this.id = id;
    }
}
